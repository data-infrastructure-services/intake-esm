#!/usr/bin/python

import intake
import unittest

class TestCatalog(unittest.TestCase):
    def test_load_catalog(self):
        col_url = "/home/k/k204210/volume/data-infrastructure-services/intake-esm/esm-collections/disk-access/dkrz_cordex_disk.json"
        self.assertTrue(intake.open_esm_datastore(col_url),
                        msg="Intake could not load catalog '{0}'".format(col_url))
    def test_load_cmipScenario(self):
        col_url = "/home/k/k204210/volume/data-infrastructure-services/intake-esm/esm-collections/disk-access/dkrz_cordex_disk.json"
        col = intake.open_esm_datastore(col_url)
        cat = col.search(experiment_id=["historical"])
        noOfExps = cat.unique(columns=["model_id"])["model_id"]["count"]
        self.assertTrue(noOfExps >= 2,
                       msg="Number of models in intake catalog for"
                       "historical is lower than 2.")
    def test_len_catalog(self):
        col_url = "/home/k/k204210/volume/data-infrastructure-services/intake-esm/esm-collections/disk-access/dkrz_cordex_disk.json"
        col = intake.open_esm_datastore(col_url)
        len_new = len(col.df)
        col_url_old="/home/k/k204210/volume/data-infrastructure-services/intake-esm/mistral-cordex_old.json"
        col_old=intake.open_esm_datastore(col_url_old)
        len_old = len(col.df)
        self.assertTrue(len_new >= len_old,
                       msg="New cataolg file contains less datasets than the old one.")
        
if __name__ == '__main__':
    unittest.main()
