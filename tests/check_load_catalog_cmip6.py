#!/usr/bin/python

import intake
import unittest
import pandas as pd
import json

class TestCatalog(unittest.TestCase):
    usecols=['project', 'activity_id', 'institution_id',
    'source_id', 'experiment_id', 'member_id', 'table_id', 'variable_id',
    'grid_label', 'dcpp_init_year', 'version', 'time_range','format', 'uri']
    
    def test_load_catalog(self):
        col_url = "/home/k/k204210/volume/data-infrastructure-services/intake-esm/esm-collections/disk-access/dkrz_cmip6_disk.json"
        with open(col_url, "r") as f:
            col_json=json.load(f)
        
        cs=10 ** 6
        for testdf in pd.read_csv("/home/k/k204210/volume/data-infrastructure-services/intake-esm/catalogs/dkrz_cmip6_disk.csv.gz",
                                 chunksize=cs) :        
            self.assertTrue(intake.open_esm_datastore(testdf,
                                                      esmcol_data=col_json),
                            msg="Intake could not load catalog '{0}'".format(col_url))
    def test_load_cmipScenario(self):
        col_url = "/home/k/k204210/volume/data-infrastructure-services/intake-esm/esm-collections/disk-access/dkrz_cmip6_disk.json"

        with open(col_url, "r") as f:    
            col_json=json.load(f)

        testdf=pd.read_csv("/home/k/k204210/volume/data-infrastructure-services/intake-esm/catalogs/dkrz_cmip6_disk.csv.gz",
                                usecols=self.usecols)
        col = intake.open_esm_datastore(testdf,esmcol_data=col_json)
        cat = col.search(source_id=["MPI-ESM1-2-HR", "AWI-CM-1-2-HR"], activity_id=["CMIP", "ScenarioMIP"])
        noOfExps = cat.unique(columns=["experiment_id"])["experiment_id"]["count"]
        self.assertTrue(noOfExps >= 9,
                       msg="Number of experiments in intake catalog for DICAD-Sources and"
                       "CMIP-Experiments is lower than 9.")
    def test_len_catalog(self):
        col_url = "/home/k/k204210/volume/data-infrastructure-services/intake-esm/esm-collections/disk-access/dkrz_cmip6_disk.json"
        with open(col_url, "r") as f:    
            col_json=json.load(f)

        testdf=pd.read_csv("/home/k/k204210/volume/data-infrastructure-services/intake-esm/catalogs/dkrz_cmip6_disk.csv.gz",
                          usecols=self.usecols)
        col = intake.open_esm_datastore(testdf,esmcol_data=col_json)
        len_new = len(col.df)
        col_url_old="/home/k/k204210/volume/data-infrastructure-services/intake-esm/tests/dkrz_cmip6_disk_old.json"
        col_old=intake.open_esm_datastore(col_url_old)
        len_old = len(col.df)
        self.assertTrue(len_new >= len_old,
                       msg="New cataolg file contains less datasets than the old one.")
        
if __name__ == '__main__':
    unittest.main()
