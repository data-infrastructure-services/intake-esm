#!/usr/bin/python

import intake
import unittest

class TestCatalog(unittest.TestCase):
    def test_load_catalog(self):
        col_url = "/home/k/k204210/volume/data-infrastructure-services/intake-esm/esm-collections/disk-access/dkrz_cmip5_disk.json"
        self.assertTrue(intake.open_esm_datastore(col_url),
                        msg="Intake could not load catalog '{0}'".format(col_url))
        
if __name__ == '__main__':
    unittest.main()
