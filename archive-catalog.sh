#!/bin/bash
set -e
project=$1
#path with /mnt/mount2
path=$2
newcatalogzip=/home/k/k204210/volume/data-infrastructure-services/intake-esm/catalogs/dkrz_${project}_disk.csv.gz
oldcatalogzip=${path}/Catalogs/dkrz_${project}_disk.csv.gz
oldcatalogArchDir="${path}/Catalogs/archive"
mkdir -p ${oldcatalogArchDir}
oldcatalogArchzip=${path}/Catalogs/archive/dkrz_${project}_disk_$(date +"%Y-%m-%d" -d "yesterday").csv.gz
#
chmod 774 ${newcatalogzip}
cp ${oldcatalogzip} ${oldcatalogArchzip}
cp $newcatalogzip $oldcatalogzip
if [ $? != 0 ]; then
  cp ${oldcatalogArchzip} ${oldcatalogzip}
else
  datemon=$(date +"%m")
  mon=$((datemon-1))
  if [ $mon == 0 ]; then
    mon=12
  fi
  printf -v mon "%02d" $mon
  rm -f $(ls -d ${oldcatalogArchDir}/* | grep -v "\-${mon}\-") #-f for if no file exists, -d for entire path
fi
