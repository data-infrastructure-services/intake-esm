{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Tutorial on how to create `intake-esm` collections and catalogs\n",
    "\n",
    "**Intake** is a cataloging tool for data repositories. It opens catalogs with *driver*s. Drivers can be plug-ins like `intake-esm`.\n",
    "\n",
    "This tutorial gives insight into the creation of a **intake-esm catalogs**. We recommend this specific driver for intake when working with ESM-data as the plugin allows to load the data with the widely used and accepted tool `xarray`.\n",
    "\n",
    "#### Requirements\n",
    "\n",
    "- [pandas](https://pandas.pydata.org/)\n",
    "- [json](https://de.wikipedia.org/wiki/JavaScript_Object_Notation)\n",
    "- [xarray](http://xarray.pydata.org/en/stable/)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### When should I create `intake-esm` collections?\n",
    "\n",
    "Cataloging your data repository with a *static* catalog for *easy access* is beneficial if\n",
    "- the data base is *stable* such that you do not have to update the content of the catalog to make it usable at all\n",
    "- the data base is *very large* such that browsing and accessing data via file system is less performant\n",
    "- the data base should be *shared* with many people such that you cannot use a data base format"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### ESM catalog definition\n",
    "\n",
    "The catalog is a combination of two entities. You might want to separate them into two files:\n",
    "\n",
    "- a **catalog file** or **catalog dict** where each entry/line corresponds to one *asset* where an asset is most usually one file. We recommend to specify the exact *file path* in one column of a line while additional meta data can be supplied in further columns to describe each file.\n",
    "- a descriptor file for the list, which is named the actual **collection** file. It contains additional settings which tell `intake` how to interprete the data. Usually, `intake-esm` collection files are `.json` formatted.\n",
    "\n",
    "The collection file will be used in `intake`: \n",
    "```python\n",
    "intake.open_esm_datastore(collection_file)\n",
    "```\n",
    "while the **catalog file** can be referenced within the **collection file**:\n",
    "```json\n",
    "  \"catalog_file\": \"/mnt/lustre02/work/ik1017/Catalogs/dkrz_cmip6_disk_netcdf.csv.gz\"\n",
    "``` \n",
    "and contains lines with *comma separated* columns, e.g.:\n",
    "\n",
    "    activity_id,institution_id,source_id,experiment_id,member_id,table_id,variable_id,grid_label,dcpp_init_year,version,time_range,path,opendap_url\n",
    "    AerChemMIP,BCC,BCC-ESM1,hist-piAer,r1i1p1f1,AERmon,c2h6,gn,,v20200511,185001-201412,/mnt/lustre02/work/ik1017/CMIP6/data/CMIP6/AerChemMIP/BCC/BCC-ESM1/hist-piAer/r1i1p1f1/AERmon/c2h6/gn/v20200511/c2h6_AERmon_BCC-ESM1_hist-piAer_r1i1p1f1_gn_185001-201412.nc,http://esgf3.dkrz.de/thredds/dodsC/cmip6/AerChemMIP/BCC/BCC-ESM1/hist-piAer/r1i1p1f1/AERmon/c2h6/gn/v20200511/c2h6_AERmon_BCC-ESM1_hist-piAer_r1i1p1f1_gn_185001-201412.nc`\n",
    "\n",
    "For keeping clear overview, you better use the same name for both files (but with different suffixes of course)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Another approach is to provide the content of the **catalog file** within in the **collection file** in the attribute *catalog_dict* where each line of a catalog file is given as a `dict`ionary:\n",
    "\n",
    "```json    \n",
    "    \"catalog_dict\": [\n",
    "        {\n",
    "            \"filename\": \"/work/mh0287/m221078/prj/switch/icon-oes/experiments/khwX155/outdata/khwX155_atm_mon_18500101.nc\",\n",
    "            \"frequency\": \"P1M\",\n",
    "            \"level_type\": \"ml\",\n",
    "            \"operation\": \"mean\",\n",
    "            \"time_range\": \"1850-01-01T00:00:00.000-1850-12-31T23:45:00.000\",\n",
    "            \"variable\": [\n",
    "                \"tas_gmean\",\n",
    "                \"rsdt_gmean\"\n",
    "            ]\n",
    "        }\n",
    "    ]\n",
    "```\n",
    "\n",
    "While having only to care for one collection file instead of two collection and catalog files, there are disadvantages coming with that appraoch:\n",
    "- you cannot easily compress the catalog\n",
    "- you can only use **one type of data access** for the catalog content. For CMIP6, we can provide access via `netcdf` or via `opendap`. We can create two collections for the same catalog file for covering both use cases."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### ESM catalog creation\n",
    "\n",
    "<div class=\"alert alert-info\">\n",
    "    <b>Note:</b> For data managemant purposes in general, we highly recoomend to define a <i>path_template</i> and a <i>filename_template</i> for a clear directory structure before storing any data.\n",
    "</div>\n",
    "\n",
    "\n",
    "#### Building the catalog file\n",
    "\n",
    "- **Define the columns**: Columns of the source list should allow to *uniquely identify* one asset/file from the catalog in order to allow effective browsing in the catalog. You can orient by the *filename_template* and *path_template* of your data repository. One column should specify the exact file path. Depending on the project, additional columns can be created which can contain any helping specification. E.g., for CMIP6, we added a `OpenDAP` column which allows users to access data from everywhere via `http`\n",
    "\n",
    "- Write a **builder** script. A typical builder for a community project contains the following sequence:\n",
    "    1. Create one or more **lists of files** based on a `find` shell command on the data base directory.\n",
    "    1. Read the lists of files and create a `panda`s DataFrame for these files.\n",
    "    1. Parse the file names and file paths and fill column values. That can be easily done by deconstructing filepaths and filenames into their parts assuming you defined a mandatory\n",
    "        - Filenames that cannot be parsed should be sorted out\n",
    "    1. The data frame is saved as the final **catalog** as a `.csv` file. You can also compress it to `.csv.gz`.\n",
    "    \n",
    "At DKRZ, we run scripts for project data on disk repeatedly in cronjobs to keep the catalog updated."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Builder tool examples\n",
    "\n",
    "- The NCAR [builder tool](https://github.com/NCAR/intake-esm-datastore/tree/e253f184ccc78906a08f1580282da070b898957a/builders) for community projects like CMIP6 and CMIP5.\n",
    "- DKRZ builder notebooks (based on NCAR tools) like this [Era5 notebook](https://gitlab.dkrz.de/data-infrastructure-services/intake-esm/-/blob/master/builder/notebooks/dkrz_era5_disk_catalog.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Creation and configuration of the **collection file**\n",
    "\n",
    "The collection file for `intake-esm` is `.json` formatted. It contains all necessary information understand the catalog including the instructions for loading the catalog file and its content. That makes the catalog *self-descriptive*. One goal of the collection file creation should be to make access of the data in catalog as **analysis ready** as possible.\n",
    "\n",
    "The entries in the `.json` file comprise:\n",
    "\n",
    "- Basic metadata for the collection: *id*, *description*, *catalog_file*\n",
    "```json\n",
    "  \"esmcat_version\": \"0.1.0\",\n",
    "  \"id\": \"mistral-cmip6\",\n",
    "  \"description\": \"This is an ESM collection for CMIP6 data accessible on the DKRZ's MISTRAL disk storage system in /work/ik1017/CMIP6/data/CMIP6\",\n",
    "  \"catalog_file\": \"/mnt/lustre02/work/ik1017/Catalogs/dkrz_cmip6_disk_netcdf.csv.gz\",\n",
    "```\n",
    "    - Note that the *catalog_file* can also be an URL so that you can host both the collection and catalog in the cloud as [DKRZ](https://swiftbrowser.dkrz.de/public/dkrz_a44962e3ba914c309a7421573a6949a6/intake-esm/ ) does.\n",
    "- Specifications for the columns from the catalog file that should be used for the collection. These are given as a list of `dict`ionaries for entry *attributes*. E.g.:\n",
    "```json\n",
    "\"attributes\": [\n",
    "    {\n",
    "      \"column_name\": \"activity_id\",\n",
    "      \"vocabulary\": \"https://raw.githubusercontent.com/WCRP-CMIP/CMIP6_CVs/master/CMIP6_activity_id.json\"\n",
    "    },\n",
    "    {\n",
    "      \"column_name\": \"source_id\",\n",
    "      \"vocabulary\": \"https://raw.githubusercontent.com/WCRP-CMIP/CMIP6_CVs/master/CMIP6_source_id.json\"\n",
    "    }\n",
    "    ]\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "- *assets* describes the column of the *file* in an `intake-esm` catalog. E.g.\n",
    "```json\n",
    "  \"assets\": {\n",
    "    \"column_name\": \"path\",\n",
    "    \"format\": \"netcdf\"\n",
    "  },\n",
    "```\n",
    "- *aggregation_control* is a `dict`ionary that defines how `xarray` can aggregate, merge or concat files to larger datasets. E.g.:\n",
    "```json\n",
    "  \"aggregation_control\": {\n",
    "    \"variable_column_name\": \"variable_id\",\n",
    "    \"groupby_attrs\": [\n",
    "      \"activity_id\",\n",
    "      \"institution_id\"\n",
    "    ],\n",
    "    \"aggregations\": [\n",
    "      {\n",
    "        \"type\": \"union\",\n",
    "        \"attribute_name\": \"variable_id\"\n",
    "      }\n",
    "  }\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "You can choose between three different aggregation *types*:\n",
    "- *\"type\": \"union\"*: will create a new dataset    \n",
    "- *\"type\": \"join_new\"*: will create a new dimension\n",
    "```json\n",
    "        \"type\": \"join_new\",\n",
    "        \"attribute_name\": \"member_id\",\n",
    "        \"options\": { \"coords\": \"minimal\", \"compat\": \"override\" }\n",
    "```\n",
    "- *\"type\": \"join_existing\"*: will append data to a specified dimension.\n",
    "```json\n",
    "        \"type\": \"join_existing\",\n",
    "        \"attribute_name\": \"time_range\",\n",
    "        \"options\": { \"dim\": \"time\", \"coords\": \"minimal\", \"compat\": \"override\" }\n",
    "```\n",
    "\n",
    "These types come from the keyword arguments of `xarray`'s `open_dataset` function."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<div class=\"alert alert-info\">\n",
    "    <b>Note:</b> It is not possible to pre-configure `dask` options for `xarray`. Be sure that users of your catalog know if and how to set <b>chunks</b>.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Multivariable assets\n",
    "\n",
    "If an asset/file contains more than one variable, `intake-esm` also features pre-selection of a variable before loading the data. [Here](https://intake-esm.readthedocs.io/en/latest/user-guide/multi-variable-assets.html) is a user guide on how to configure the collection for that.\n",
    "\n",
    "1. the *variable_column* of the catalog must contain iterables (`list`, `tuple`, `set`) of values.\n",
    "2. the user must specifiy a dictionary of functions for converting values in certain columns into iterables. This is done via the `csv_kwargs` argument such that the collection needs to be opened as follows:\n",
    "\n",
    "```python\n",
    "import ast\n",
    "import intake\n",
    "\n",
    "col = intake.open_esm_datastore(\n",
    "    \"multi-variable-collection.json\",\n",
    "    csv_kwargs={\"converters\": {\"variable\": ast.literal_eval}},\n",
    ")\n",
    "col\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Access the collection's meta data\n",
    "\n",
    "The content of the **Collection file** is saved in the attribute `esmcol_data`, e.g.:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'esmcat_version': '0.1.0',\n",
       " 'id': 'DKRZ-CMIP6-fromcloud',\n",
       " 'description': \"This is an ESM collection for CMIP6 data accessible on the DKRZ's mistral disk system \",\n",
       " 'catalog_file': 'https://swift.dkrz.de/v1/dkrz_a44962e3ba914c309a7421573a6949a6/intake-esm/dkrz_cmip6_disk_netcdf.csv.gz',\n",
       " 'attributes': [{'column_name': 'activity_id',\n",
       "   'vocabulary': 'https://raw.githubusercontent.com/WCRP-CMIP/CMIP6_CVs/master/CMIP6_activity_id.json'},\n",
       "  {'column_name': 'source_id',\n",
       "   'vocabulary': 'https://raw.githubusercontent.com/WCRP-CMIP/CMIP6_CVs/master/CMIP6_source_id.json'},\n",
       "  {'column_name': 'institution_id',\n",
       "   'vocabulary': 'https://raw.githubusercontent.com/WCRP-CMIP/CMIP6_CVs/master/CMIP6_institution_id.json'},\n",
       "  {'column_name': 'experiment_id',\n",
       "   'vocabulary': 'https://raw.githubusercontent.com/WCRP-CMIP/CMIP6_CVs/master/CMIP6_experiment_id.json'},\n",
       "  {'column_name': 'member_id', 'vocabulary': ''},\n",
       "  {'column_name': 'table_id',\n",
       "   'vocabulary': 'https://raw.githubusercontent.com/WCRP-CMIP/CMIP6_CVs/master/CMIP6_table_id.json'},\n",
       "  {'column_name': 'variable_id', 'vocabulary': ''},\n",
       "  {'column_name': 'grid_label',\n",
       "   'vocabulary': 'https://raw.githubusercontent.com/WCRP-CMIP/CMIP6_CVs/master/CMIP6_grid_label.json'},\n",
       "  {'column_name': 'version', 'vocabulary': ''},\n",
       "  {'column_name': 'dcpp_start_year', 'vocabulary': ''}],\n",
       " 'assets': {'column_name': 'path', 'format': 'netcdf'},\n",
       " 'aggregation_control': {'variable_column_name': 'variable_id',\n",
       "  'groupby_attrs': ['activity_id',\n",
       "   'institution_id',\n",
       "   'source_id',\n",
       "   'experiment_id',\n",
       "   'table_id',\n",
       "   'grid_label'],\n",
       "  'aggregations': [{'type': 'union', 'attribute_name': 'variable_id'},\n",
       "   {'type': 'join_existing',\n",
       "    'attribute_name': 'time_range',\n",
       "    'options': {'dim': 'time', 'coords': 'minimal', 'compat': 'override'}},\n",
       "   {'type': 'join_new',\n",
       "    'attribute_name': 'member_id',\n",
       "    'options': {'coords': 'minimal', 'compat': 'override'}},\n",
       "   {'type': 'join_new',\n",
       "    'attribute_name': 'dcpp_init_year',\n",
       "    'options': {'coords': 'minimal', 'compat': 'override'}}]}}"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import intake\n",
    "\n",
    "dkrz_cdp=intake.open_catalog(\"https://swift.dkrz.de/v1/dkrz_a44962e3ba914c309a7421573a6949a6/intake-esm/dkrz_data-pool_cloudcatalog.yaml\")\n",
    "list(dkrz_cdp)\n",
    "esm_col=dkrz_cdp.dkrz_cmip6_disk_netcdf_fromcloud\n",
    "esm_col.esmcol_data\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "taucenv",
   "language": "python",
   "name": "taucenv"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
