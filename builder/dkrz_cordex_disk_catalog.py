#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import dask
import fnmatch
import dask.dataframe as dd
from intake.source.utils import reverse_format
import os
import re
import subprocess
from tqdm.auto import tqdm
from pathlib import Path
import shutil
import numpy as np
import itertools
from functools import lru_cache


# In[17]:

@lru_cache(maxsize=None)
def get_file_list():
#    persist_path = Path(persist_path)
#    if persist_path.exists():
#        shutil.rmtree(persist_path)
#    persist_path.mkdir()
    #root = Path("/mnt/lustre02/work/ik1017/CMIP6/data/CMIP6")
    sources=["/work/kd0956/CORDEX/data/cordex",
            "/work/ik1017/CORDEX/data/cordex",
            "/work/kd0956/REKLIES/data/cordex-reklies",
            "/work/ik1017/C3SCORDEX/data/c3s-cordex"]
    dirs=[]
    depth=4
    for sourcedir in sources:
        root = Path(sourcedir)
        pattern = '*/' * (depth + 1)
        dirs += [x for x in root.glob(pattern) if x.is_dir()]
        
    print(len(dirs))
    from dask.diagnostics import ProgressBar
    @dask.delayed
    def _file_dir_files(directory):
        try:
            cmd = ['find', '-L', directory.as_posix(), '-name', "*.nc", "-perm", "-444"]
            proc = subprocess.Popen(cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
            output = proc.stdout.read().decode('utf-8').split()
        except Exception:
            output = []
        return output

    print('Getting list of assets...\n')
    filelist = [_file_dir_files(directory) for directory in dirs]
    # watch progress
    with ProgressBar():
        filelist = dask.compute(*filelist)

    filelist = list(itertools.chain(*filelist))
    return filelist


# In[18]:


filelist=get_file_list()




# In[ ]:


def _reverse_filename_format(file_basename, filename_template=None, gridspec_template=None):
    """
    Uses intake's ``reverse_format`` utility to reverse the string method format.
    Given format_string and resolved_string, find arguments
    that would give format_string.format(arguments) == resolved_string
    """
    try:
        return reverse_format(filename_template, file_basename)
    except ValueError:
        try:
            return reverse_format(gridspec_template, file_basename)
        except:
            print(
                f'Failed to parse file: {file_basename} using patterns: {filename_template} and {gridspec_template}'
            )
            return {}
            
def _extract_attr_with_regex(input_str, regex, strip_chars=None):
    pattern = re.compile(regex, re.IGNORECASE)
    match = re.findall(pattern, input_str)
    if match:
        match = max(match, key=len)
        if strip_chars:
            match = match.strip(strip_chars)

        else:
            match = match.strip()

        return match

    else:
        return None
    

exclude_patterns = ['*/files/*', '*/latest/*']
def _filter_func(path):
    return not any(
        fnmatch.fnmatch(path, pat=exclude_pattern) for exclude_pattern in exclude_patterns
    )


# In[ ]:


filelist = list(filter(_filter_func, filelist))


# In[ ]:


len(filelist)


# In[ ]:


def get_attrs(filepath):
    basename = os.path.basename(filepath)
    dirname = os.path.dirname(filepath)
#    filename_template = '{variable_id}_{table_id}_{source_id}_{experiment_id}_{member_id}_{grid_label}_{time_range}.nc'
    filename_template = '{variable_id}_{CORDEX_domain}_{driving_model_id}_{experiment_id}_{member}_{model_id}_{rcm_version_id}_{frequency}_{time_range}.nc'


#    gridspec_template = (
#                '{variable_id}_{table_id}_{source_id}_{experiment_id}_{member_id}_{grid_label}.nc'
#            )

    gridspec_template = (
                '{variable_id}_{CORDEX_domain}_{driving_model_id}_{experiment_id}_{member}_{model_id}_{rcm_version_id}_{frequency}.nc'
            )

    f = _reverse_filename_format(
            basename, filename_template=filename_template, gridspec_template=gridspec_template
        )

    fileparts = {}
    fileparts.update(f)
    parent = os.path.dirname(filepath).strip('/')
#    parent_split = parent.split(f"/{fileparts['source_id']}/")
    parent_split = parent.split(f"/{fileparts['driving_model_id']}/")
    part_1 = parent_split[0].strip('/').split('/')
#    grid_label = parent.split(f"/{fileparts['variable_id']}/")[1].strip('/').split('/')[0]
#    frequency = parent.split(f"/{fileparts['variable_id']}/")[1].strip('/').split('/')[0]
#    fileparts['grid_label'] = grid_label
#    fileparts['frequency'] = frequency
#    fileparts['activity_id'] = part_1[-2]
    fileparts['institute_id'] = part_1[-1]
    fileparts['product_id'] = part_1[-3]
    version_regex = r'v\d{4}\d{2}\d{2}|v\d{1}'
    version = _extract_attr_with_regex(parent, regex=version_regex) or 'v0'
    fileparts['version'] = version
    fileparts['path'] = filepath
    return fileparts 


# In[ ]:


entries = list(map(get_attrs, filelist))


# In[ ]:


entries[0]


# In[ ]:


len(entries)


# In[ ]:


df1 = pd.DataFrame(entries)
df1.head()


# In[ ]:


# Some entries are invalid
products=["output"]
invalids = df1[~df1.product_id.isin(products)]
df = df1[df1.product_id.isin(products)]
invalids


# In[43]:


with open('/home/k/k204210/volume/data-infrastructure-services/intake-esm/invalids-cordex.txt', 'w') as f :
    for file in invalids.path.values :
        f.write(file+"\n")


# ## Pick the latest versions

# In[ ]:


grpby = list(set(df.columns.tolist()) - {'path', 'version', 'time_range'})
print(grpby)
groups = df.groupby(grpby)


# In[44]:


idx_to_remove = []
for _, group in groups:
    if group.version.nunique() > 1:
        recentVersion=group.sort_values(by=['version'], ascending=False)["version"].iloc[0]
        idx_to_remove.extend(group[group["version"]!= recentVersion].index[:].values.tolist())


# In[45]:


len(idx_to_remove)


# In[46]:


len(df)


# In[47]:


df = df.drop(index=idx_to_remove)
len(df)


# In[48]:

df = df[~df[list(set(df.columns.tolist()) - {'path'})].duplicated(keep="first")]

df.head()


# In[19]:


def get_opendap(row):
    filename=row["path"]
    opendaptrunk="/".join(filename.split("/")[7:])
    return "http://esgf1.dkrz.de/thredds/dodsC/cordex/"+opendaptrunk


# In[ ]:


df["opendap_url"] =df.apply(lambda row: get_opendap(row), axis=1)
df.loc[:,"project"]="CORDEX"
df.loc[:,"institution_id"]=df["institute_id"]
df.loc[:,"simulation_id"]=df["member"]
df.loc[:,"source_id"]=df["model_id"]
df.loc[:,"grid_label"]="None"
df.loc[:,"grid_id"]="None"
df.loc[:,"realm"]="None"
df.loc[:,"level_type"]="None"
df.loc[:,"time_reduction"]="None"
df.loc[:,"format"]="netcdf"
df.loc[:,"uri"]=df["path"]
df[["time_min","time_max"]]=df["time_range"].str.split('-',expand=True,n=1)

columns = ["project",
           "product_id",
           "CORDEX_domain",
           "institute_id",
           "driving_model_id",
           "experiment_id",
           "member",
           "model_id",
           "rcm_version_id",
           "frequency", 
           "variable_id", 
           "version",
           "time_range", 
           "uri",
           "institution_id",
           "source_id",
           "simulation_id",
           "grid_label",
           "grid_id",
           "time_reduction",
           "realm",
           "level_type",
           "time_min", 
           "time_max", 
           "path",
           "format",
           "opendap_url"]

df = df[columns]
df = df.sort_values(columns, ascending = True).reset_index(drop=True)
df.head()


# In[50]:


#df.to_csv("./mistral-cmip6.csv.gz", compression="gzip", index=False)
df.to_csv("/home/k/k204210/volume/data-infrastructure-services/intake-esm/catalogs/dkrz_cordex_disk.csv.gz", compression="gzip", index=False)


# In[ ]:


len(df)


# In[ ]:




