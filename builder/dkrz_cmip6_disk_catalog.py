import sys
sys.path.insert(0, '/home/k/k204210/volume/data-infrastructure-services/intake-esm/builder/ncar-builder/builders/')
from cmip import build_cmip
import pandas as pd
from tqdm import tqdm
import ast
import json

root_path="/work/ik1017/CMIP6/data/CMIP6"
depth=4
pick_latest_version="y"
cmip_version=6
csv_filepath="/home/k/k204210/intake-builder/catalogs/dkrz_cmip6_disk-temp.csv.gz"

df = build_cmip(root_path, cmip_version, depth=depth, pick_latest_version=pick_latest_version)

df.to_csv(csv_filepath, compression='gzip', index=False)

# Add PIDs and OpenDAPs to catalog

df=pd.read_csv(csv_filepath)
pids=pd.read_csv("/home/k/k204210/volume/data-infrastructure-services/intake-esm/catalogs/dkrz_cmip6_disk_filepids.csv.gz",
                low_memory=True)
pids["file_pids"]=pids["file_pids"].str.replace("'",'"')
pids_dict=[]
for index,row in tqdm(pids.iterrows()):
    try:
        file_pids_dict=ast.literal_eval(row["file_pids"])
        for file_pid, filename in file_pids_dict.items() :
            pids_dict.append([row["path"]+os.sep+filename, file_pid])
    except:
        pass

pids_df=pd.DataFrame.from_records(pids_dict, columns=["path", "file_pid"])
pids_df["path"]=pids_df["path"].str.replace("lustre02","lustre")
df=df.merge(pids_df, how='left', on='path')

df.to_csv("/home/k/k204210/volume/data-infrastructure-services/intake-esm/catalogs/dkrz_cmip6pids_disk.csv.gz", compression="gzip", index=False)

df.drop(columns="file_pid", inplace=True)

del pids
del pids_df

df["path"]=df["path"].str.replace("/mnt/lustre/",'/')
# Add long name information

catalog_basedir="/work/ik1017/Catalogs"
c6tables_basedir="/home/k/k204210/volume/dicad/cdo-incl-cmor/configuration/cmip6/cmip6-cmor-tables/Tables/"

mip_tables={}
for table in df["table_id"].unique():
    with open(f"{c6tables_basedir}CMIP6_{table}.json", "r") as f:
        mip_tables[table]=json.load(f)
        
df["project"]="CMIP6"
df["simulation_id"]=df["member_id"]
df["grid_id"]="No entry"
groups=df.groupby(["table_id","variable_id"])
for key,group in tqdm(groups):
    table_f=mip_tables[key[0]]
    varname=key[1]
    try :
        df.loc[group.index,"frequency"]=table_f["variable_entry"][varname]["frequency"]
        df.loc[group.index,"time_reduction"]=table_f["variable_entry"][varname]["cell_methods"]
        df.loc[group.index,"long_name"]=table_f["variable_entry"][varname]["long_name"]
        df.loc[group.index,"units"]=table_f["variable_entry"][varname]["units"]
        df.loc[group.index,"realm"]=table_f["variable_entry"][varname]["modeling_realm"]
        df.loc[group.index,"dimensions"]=table_f["variable_entry"][varname]["dimensions"]
    except:
        #cannot do it for outname!=entry
        pass    

# Add level types
    
generic_levels=[]
for k,v in mip_tables.items():
    levels=v["Header"]["generic_levels"]
    if not levels:
        continue
    if ' ' in levels:
        levels=levels.split(' ')
    else:
        levels=[levels]
    generic_levels=list(set(generic_levels+levels))
    
for levtype in generic_levels:
    df.loc[df["dimensions"].str.contains(levtype, na=False),"level_type"]=levtype

df=df.drop(columns=["dimensions"])
df.loc[:,"time_reduction"]=df["time_reduction"].str.split('time:').str[1]
df[["time_min","time_max"]]=df["time_range"].str.split('-',expand=True,n=1)
df["format"]="netcdf"
df["uri"]=df["path"]

# Add opendapurl

def get_opendap(row):
    #filename=row["path"]
    filename=row
    opendaptrunk="/".join(filename.split("/")[6:])
#    try:
#        opendapurl="http://esgf3.dkrz.de/thredds/dodsC/cmip6/"+opendaptrunk
#        headers=requests.head(opendapurl).headers
#        if 'opendap' in headers.get('XDODS-Server', '') :
#            return opendapurl
#        else:
#            return np.nan
#    except:
#        return np.nan
    return "http://esgf3.dkrz.de/thredds/dodsC/cmip6/"+opendaptrunk

# compress
category_cols = ['activity_id', 'institution_id',
    'source_id', 'experiment_id', 'member_id', 'table_id', 'variable_id',
    'grid_label',
    'project', 'simulation_id', 'grid_id', 'frequency',
    'time_reduction', 'long_name', 'units', 'realm',
    'format']

df=df.astype({key: "category"
    for key in category_cols
    })

df["opendap_url"] =df["path"].map(lambda row: get_opendap(row))

columns = ['activity_id', 'institution_id',
    'source_id', 'experiment_id', 'member_id', 'table_id', 'variable_id',
    'grid_label', 'dcpp_init_year', 'version', 'time_range', 'path',
    'opendap_url', 'project', 'simulation_id', 'grid_id', 'frequency',
    'time_reduction', 'long_name', 'units', 'realm', 'level_type', 'time_min',
    'time_max', 'format', 'uri']

df = df[columns]
df = df.sort_values(columns, ascending = True).reset_index(drop=True)
df.to_csv("/home/k/k204210/volume/data-infrastructure-services/intake-esm/catalogs/dkrz_cmip6_disk.csv.gz", compression="gzip", index=False)
