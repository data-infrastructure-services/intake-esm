import sys
sys.path.insert(0, '/home/k/k204210/volume/data-infrastructure-services/intake-esm/builder/ncar-builder/builders/')
from cmip import build_cmip
import pandas as pd
from tqdm import tqdm
import ast

root_path="/work/kd0956/CMIP5/data/cmip5"
depth=4
pick_latest_version="y"
cmip_version=5
csv_filepath="/home/k/k204210/volume/data-infrastructure-services/intake-esm/catalogs/dkrz_cmip5_disk.csv.gz"

df = build_cmip(root_path, cmip_version, depth=depth, pick_latest_version=pick_latest_version)

print(len(df["temporal_subset"]))

df["path"]=df["path"].str.replace("/mnt/lustre/",'/')

df["project"]="cmip5"
df["institution_id"]=df["institute"]
df["source_id"]=df["model"]
df["simulation_id"]=df["ensemble_member"]
df["realm"]=df["modeling_realm"]
df["experiment_id"]=df["experiment"]
df["time_range"]=df["temporal_subset"]
df["time_min"]=df["time_range"].str.split('-').str[0]
df["time_max"]=df["time_range"].str.split('-').str[1]
df["format"]="netcdf"
df["uri"]=df["path"]
df["variable_id"]=df["variable"]
df['grid_id']="None"
df['level_type']="None"
df['time_reduction']="None"
df['grid_label']="None"

save_columns = ["project",
             "product_id",
             "institute",
             "model",
             "experiment",
             "frequency",
             "modeling_realm",
             "mip_table",
             "ensemble_member",
             "version",
             "variable",
             "temporal_subset",
                "institution_id",
                "source_id",
             "experiment_id",
             "variable_id",
             "grid_label",
               "realm",
               "level_type",
             "time_range",
             "time_min",
             "time_max",
             "simulation_id",
             "grid_id",
             "time_reduction",
             "format",
                "uri"]

df = df[save_columns]
df = df.sort_values(save_columns, ascending = True).reset_index(drop=True)
df.to_csv(csv_filepath, compression='gzip', index=False)
