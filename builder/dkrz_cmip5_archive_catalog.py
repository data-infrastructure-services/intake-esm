import pandas as pd
#file from andrej
cmip5df_from_ceratsv=pd.read_csv("/home/k/k204210/intake-esm/catalogs/cera.tsv",
                                names=["jblob_file"])
columns=["project",
         "product_id",
         "institute",
         "model",
         "experiment",
         "frequency",
         "modeling_realm",
         "mip_table",
         "ensemble_member",
         "version",
         "variable",
         "temporal_subset",
         "nc"]
columns_dict={
    k:v
    for k,v in enumerate(columns)
}
cmip5df_from_ceratsv_split=cmip5df_from_ceratsv["jblob_file"].str.split('.',
                                             expand=True)
cmip5df_from_ceratsv_split=cmip5df_from_ceratsv_split.rename(columns=columns_dict)
cmip5df_from_ceratsv_split["temporal_subset"]=cmip5df_from_ceratsv_split["temporal_subset"].str.split('_').str[5]
cmip5df_from_ceratsv_split=cmip5df_from_ceratsv_split.drop(columns=["nc"])
cmip5df_from_ceratsv["jblob_file"]=cmip5df_from_ceratsv["jblob_file"].str.replace('.','/').str.replace('/nc','.nc')
cmip5df=cmip5df_from_ceratsv.join(cmip5df_from_ceratsv_split)

cmip5df.loc[:,"institution_id"]=cmip5df["institute"]
cmip5df.loc[:,"source_id"]=cmip5df["model"]
cmip5df.loc[:,"experiment_id"]=cmip5df["experiment"]
cmip5df.loc[:,"member_id"]=cmip5df["ensemble_member"]
cmip5df.loc[:,"table_id"]=cmip5df["mip_table"]
cmip5df.loc[:,"variable_id"]=cmip5df["variable"]
cmip5df.loc[:,"grid_label"]="None"
cmip5df.loc[:,"time_range"]=cmip5df["temporal_subset"]
cmip5df[["time_min","time_max"]]=cmip5df["time_range"].str.split('-',expand=True,n=1)
cmip5df.loc[:,"simulation_id"]=cmip5df["member_id"]
cmip5df.loc[:,"grid_id"]="None"
cmip5df.loc[:,"time_reduction"]="None"
cmip5df.loc[:,"uri"]="None"
cmip5df.loc[:,"format"]="netcdf"
cmip5df.loc[:,"level_type"]="None"

save_columns = ["project",
             "product_id",
             "institute",
             "model",
             "experiment",
             "frequency",
             "modeling_realm",
             "mip_table",
             "ensemble_member",
             "version",
             "variable",
             "temporal_subset",
             "institution_id",
             "source_id",
             "experiment_id",
             "simulation_id",
             "realm,                
             "time_reduction",
             "grid_label",
             "grid_id",
             "level_type",
             "grid_id",
             "time_range",
             "time_min",
             "time_max",
             "format",
             "uri",
             "jblob_file",
             "variable_id]
cmip5df = cmip5df[save_columns]
cmip5df = cmip5df.sort_values(save_columns, ascending = True).reset_index(drop=True)
cmip5df.to_csv("/home/k/k204210/intake-esm/catalogs/dkrz_cmip5_archive.csv.gz", compression="gzip", index=False)
cmip5df.to_csv("/mnt/lustre/work/ik1017/Catalogs/Candidates/dkrz_cmip5_archive.csv.gz", compression="gzip", index=False)