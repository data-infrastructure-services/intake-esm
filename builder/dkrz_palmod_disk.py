import sys
sys.path.insert(0, '/home/k/k204210/intake-esm/builder/ncar-builder/builders/')
from cmip import build_cmip
import pandas as pd
from tqdm import tqdm
import ast
import json

root_path="/work/kd1292/ESGF_PalMod"
depth=4
pick_latest_version="y"
cmip_version=6
columns=['project',
        'institution_id',
        'source_id',
        'experiment_id',
        'member_id',
        'table_id',
        'variable_id',
        'grid_label',
        'dcpp_init_year',
        'version',
        'time_range',
        'path', #must be path when cmip_version=6
        ]
csv_filepath="/home/k/k204210/intake-esm/catalogs/dkrz_palmod2_disk.csv.gz"

df = build_cmip(root_path,
                cmip_version,
                columns=columns,
                depth=depth,
                pick_latest_version=pick_latest_version)

df.loc[:,"project"]="PalMod2"
df.loc[:,"simulation_id"]=df["member_id"]
df[["time_min","time_max"]]=df["time_range"].str.split('-',expand=True,n=1)
df.loc[:,"format"]="netcdf"
df.loc[:,"uri"]=df["path"]
df.loc[:,"grid_id"]="None"

c6tables_basedir="/work/bm0021/from_Mistral/bm0021/PalMod2/cmor_tables/"

mip_tables={}
for table in df["table_id"].unique():
    with open(f"{c6tables_basedir}PalMod2_{table}.json", "r") as f:
        mip_tables[table]=json.load(f)
        
groups=df.groupby(["table_id","variable_id"])
for key,group in tqdm(groups):
    table_f=mip_tables[key[0]]
    varname=key[1]
    try :
        df.loc[group.index,"frequency"]=table_f["variable_entry"][varname]["frequency"]
        df.loc[group.index,"time_reduction"]=table_f["variable_entry"][varname]["cell_methods"]
        df.loc[group.index,"long_name"]=table_f["variable_entry"][varname]["long_name"]
        df.loc[group.index,"units"]=table_f["variable_entry"][varname]["units"]
        df.loc[group.index,"realm"]=table_f["variable_entry"][varname]["modeling_realm"]
        df.loc[group.index,"dimensions"]=table_f["variable_entry"][varname]["dimensions"]
    except:
        #cannot do it for outname!=entry
        pass    
    
# Add level types
    
generic_levels=[]
for k,v in mip_tables.items():
    levels=v["Header"]["generic_levels"]
    if not levels:
        continue
    if ' ' in levels:
        levels=levels.split(' ')
    else:
        levels=[levels]
    generic_levels=list(set(generic_levels+levels))
    
for levtype in generic_levels:
    df.loc[df["dimensions"].str.contains(levtype, na=False),"level_type"]=levtype

df=df.drop(columns=["dimensions"])
df.loc[:,"time_reduction"]=df["time_reduction"].str.split('time:').str[1]

columns = ['project', 'institution_id',
    'source_id', 'experiment_id', 'member_id', 'table_id', 'variable_id',
    'grid_label', 'version', 'time_range',
    'simulation_id', 'grid_id', 'frequency',
    'time_reduction', 'long_name', 'units', 'realm', 'level_type', 'time_min',
    'time_max', 'format', 'path', 'uri']

df = df[columns]
df = df.sort_values(columns, ascending = True).reset_index(drop=True)
df.to_csv(csv_filepath, compression="gzip", index=False)